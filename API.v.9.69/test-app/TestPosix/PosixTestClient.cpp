#include "PosixTestClient.h"
#include "EPosixClientSocket.h"
#include "EPosixClientSocketPlatform.h"
#include "tick_type_str.h"
#include "dm.h"

///////////////////////////////////////////////////////////
// member funcs
PosixTestClient::PosixTestClient()
	: m_pClient(new EPosixClientSocket(this))
	, m_state(ST_CONNECT)
	, m_sleepDeadline(0)
	, m_orderId(0)
{
    std::stringstream for_the_record;
    for_the_record << "PosixTestClient constructor\n";
    dout << for_the_record.str();
}

PosixTestClient::~PosixTestClient()
{
    std::stringstream for_the_record;
    for_the_record << "PosixTestClient destructor\n";
    dout << for_the_record.str();
}

bool PosixTestClient::connect(const char *host, unsigned int port, int clientId)
{
    std::stringstream for_the_record;
    for_the_record << "PosixTestClient::connect()\n";
    dout << for_the_record.str();

	// trying to connect
	printf( "\t Connecting to %s:%d clientId:%d\n", !( host && *host) ? "127.0.0.1" : host, port, clientId);

	bool bRes = m_pClient->eConnect( host, port, clientId);

	if (bRes) {
		printf( "\t Connected to %s:%d clientId:%d\n", !( host && *host) ? "127.0.0.1" : host, port, clientId);
	}
	else
		printf( "\t Cannot connect to %s:%d clientId:%d\n", !( host && *host) ? "127.0.0.1" : host, port, clientId);

	return bRes;
}

void PosixTestClient::disconnect() const
{
    std::stringstream for_the_record;
    for_the_record << "PosixTestClient::disconnect()\n";
    dout << for_the_record.str();

	m_pClient->eDisconnect();

	printf ( "\t Disconnected\n");
}

bool PosixTestClient::isConnected() const
{
	//dout << "PosixTestClient::isConnected()\n";
	return m_pClient->isConnected();
}

void PosixTestClient::processMessages()
{
	fd_set readSet, writeSet, errorSet;

	struct timeval tval;
	tval.tv_usec = 0;
	tval.tv_sec = 0;

	time_t now = time(NULL);

	switch (m_state) {
		case ST_PLACEORDER:
			example_reqMktData();
			example_placeOrder();
			break;
		case ST_PLACEORDER_ACK:
			break;
		case ST_CANCELORDER:
			cancelOrder();
			break;
		case ST_CANCELORDER_ACK:
			break;
		case ST_PING:
			// reqCurrentTime(); // for forex data we wont request server time to avoid extra noise in the output
			break;
		case ST_PING_ACK:
			if( m_sleepDeadline < now) {
				disconnect();
				return;
			}
			break;
		case ST_IDLE:
			if( m_sleepDeadline < now) {
				m_state = ST_PING;
				return;
			}
			break;
	}

	if( m_sleepDeadline > 0) {
		// initialize timeout with m_sleepDeadline - now
		tval.tv_sec = m_sleepDeadline - now;
	}

	if( m_pClient->fd() >= 0 ) {

		FD_ZERO( &readSet);
		errorSet = writeSet = readSet;

		FD_SET( m_pClient->fd(), &readSet);

		if( !m_pClient->isOutBufferEmpty())
			FD_SET( m_pClient->fd(), &writeSet);

		FD_CLR( m_pClient->fd(), &errorSet);

		int ret = select( m_pClient->fd() + 1, &readSet, &writeSet, &errorSet, &tval);

		if( ret == 0) { // timeout
			return;
		}

		if( ret < 0) {	// error
			disconnect();
			return;
		}

		if( m_pClient->fd() < 0)
			return;

		if( FD_ISSET( m_pClient->fd(), &errorSet)) {
			// error on socket
			m_pClient->onError();
		}

		if( m_pClient->fd() < 0)
			return;

		if( FD_ISSET( m_pClient->fd(), &writeSet)) {
			// socket is ready for writing
			m_pClient->onSend();
		}

		if( m_pClient->fd() < 0)
			return;

		if( FD_ISSET( m_pClient->fd(), &readSet)) {
			// socket is ready for reading
			m_pClient->onReceive();
		}
	}
}

//////////////////////////////////////////////////////////////////
// methods
void PosixTestClient::reqCurrentTime()
{
    std::stringstream for_the_record;
    for_the_record << "reqCurrentTime()\n";
    dout << for_the_record.str();
}




void PosixTestClient::cancelOrder()
{
    std::stringstream for_the_record;
    for_the_record << "cancelOrder()\n";
    dout << for_the_record.str();
}

///////////////////////////////////////////////////////////////////
// events
void PosixTestClient::orderStatus( OrderId orderId, const IBString &status, int filled,
	   int remaining, double avgFillPrice, int permId, int parentId,
	   double lastFillPrice, int clientId, const IBString& whyHeld)

{
    std::stringstream for_the_record;
    for_the_record << "PosixTestClient destructor\n";

    for_the_record << "orderStatus()\n"
		   << "\t orderId: " << orderId << "\n"
		   << "\t status: " << status << "\n"
		   << "\t filled: " << filled << "\n"
		   << "\t remaining: " << remaining << "\n"
		   << "\t avgFillPrice" << avgFillPrice << "\n"
		   << "\t permId" << permId << "\n"
		   << "\t parentId" << parentId << "\n"
		   << "\t lastFillPrice" << lastFillPrice << "\n"
		   << "\t clientId" << clientId << "\n"
		   << "\t whyHeld" << whyHeld << "\n";
    dout << for_the_record.str();

    // report any abnormalities
    if (orderId <=0) {
	anomaly << for_the_record.str();
    }
}

void PosixTestClient::nextValidId( OrderId orderId)
{
    std::stringstream for_the_record;
    for_the_record << "nextValidId()\n\t orderId: " << orderId << "\n";
    dout << for_the_record.str();

    m_orderId = orderId;
    m_state = ST_PLACEORDER;

    dout << "+++ switching state to ST_PLACEORDER ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
}

void PosixTestClient::currentTime( long time)
{
    std::stringstream for_the_record;
    for_the_record << "currentTime() time=" << time << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::error(const int id, const int errorCode, const IBString errorString)
{
	
    std::stringstream for_the_record;
    for_the_record << "error():\n"
	     << "\t id: " << id << "\n"
	     << "\t errorCode: " << errorCode << "\n"
	     << "\t errorString: " << errorString << "\n";
    dout << for_the_record.str();


    if( id == -1 && errorCode == 1100) // if (id == -1 && errorCode == 1100) then "Connectivity between IB and TWS has been lost"
    	disconnect();
}

void PosixTestClient::tickPrice( TickerId tickerId, TickType tickType, double price, int canAutoExecute) {

    std::stringstream for_the_record;
    for_the_record << "tickPrice():\n"
	     << "\t tickerId: " << tickerId << " (symbol: " << stockticks[tickerId].sym << ")\n"
	     << "\t TickType: " << TickTypeStr[tickType] << " (" << tickType << ")\n"
	     << "\t price: " << price << "\n"
	     << "\t canAutoExecute: " << canAutoExecute << "\n";
    dout << for_the_record.str();
		
    // some anomalies check:
    if ( tickerId < 0) anomaly << "tickPrice(): unexpected tickerId: " << tickerId << " (can't be less than zero)\n";
    if ( tickerId >= (TickerId)stockticks.size()) anomaly << "tickPrice(): unexpected tickerId: " << tickerId << " (should not exceed stockticks.size=" << stockticks.size() << ")\n";

    time_t t = time(NULL);
    tm* timePtr = localtime(&t);
	    
    TLTick k;
    k.date = ((timePtr->tm_year+1900) * 10000) + (timePtr->tm_mon * 100) + timePtr->tm_mday;
    k.time = (timePtr->tm_hour        * 10000) + (timePtr->tm_min * 100) + timePtr->tm_sec;
    k.sym = stockticks[tickerId].sym;

    if (tickType==LAST)
    {
	stockticks[tickerId].trade = price;
	k.trade = price;
	k.size = stockticks[tickerId].size;
    }
    else if (tickType==BID)
    {
	stockticks[tickerId].bid = price;
	k.bid = stockticks[tickerId].bid;
	k.bs = stockticks[tickerId].bs;
    }
    else if (tickType==ASK)
    {
	stockticks[tickerId].ask = price;
	k.ask = stockticks[tickerId].ask;
	k.os = stockticks[tickerId].os;
    }
    else 
    {
        dout << "not relevant tick info \n";
        return; // not relevant tick info
    }

    if (k.isValid())// && needStock(k.sym))
    {
	// this->SrvGotTick(k);
	dout << "GOT_TICK: " << k.Serialize() << "\n";
    }
}

void PosixTestClient::tickSize( TickerId tickerId, TickType tickType, int size) {
    std::stringstream for_the_record;
    for_the_record << "tickSize():\n"
	     << "\t tickerId: " << tickerId << " (symbol: " << stockticks[tickerId].sym << ")\n"
	     << "\t TickType: " << TickTypeStr[tickType] << " ("<< tickType << ")\n"
	     << "\t size: " << size << "\n";
    dout << for_the_record.str();

    // some anomalies check:
    if ( tickerId < 0) anomaly << "tickSize(): unexpected tickerId: " << tickerId << " (can't be less than zero)\n";
    if ( tickerId >= (TickerId)stockticks.size()) anomaly << "tickSize(): unexpected tickerId: " << tickerId << " (should not exceed stockticks.size=" << stockticks.size() << ")\n";
    
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);
	    
    TLTick k;
    k.date = ((timePtr->tm_year+1900) * 10000) + (timePtr->tm_mon * 100) + timePtr->tm_mday;
    k.time = (timePtr->tm_hour        * 10000) + (timePtr->tm_min * 100) + timePtr->tm_sec;
    k.sym = stockticks[tickerId].sym;
//    TLSecurity sec = TLSecurity::Deserialize(k.sym);

    Contract contract = contracts[tickerId];
    bool hundrednorm = (contract.secType == "STK") || (contract.secType == "" );
    if (tickType==LAST_SIZE)
    {
        stockticks[tickerId].size = hundrednorm ? size*100 : size;
        k.trade = stockticks[tickerId].trade;
        k.size = stockticks[tickerId].size;
    }
    else if (tickType==BID_SIZE)
    {
	stockticks[tickerId].bs = size;
        k.bid = stockticks[tickerId].bid;
        k.bs = stockticks[tickerId].bs;
    }
    else if (tickType==ASK_SIZE)
    {
        stockticks[tickerId].os = size;
        k.ask= stockticks[tickerId].ask;
        k.os = stockticks[tickerId].os;
    }
    else return; // not relevant tick info
    
    if (k.isValid()) // && needStock(k.sym))
    {
	//this->SrvGotTick(k);
	dout << "GOT TICK: " << k.Serialize() << "\n"; // DM::Inspector(&k) << "\n";
    }
}

void PosixTestClient::tickOptionComputation( TickerId tickerId, TickType tickType, double impliedVol, double delta,
					 double optPrice, double pvDividend,
					 double gamma, double vega, double theta, double undPrice) {

    std::stringstream for_the_record;
    for_the_record << "tickOptionComputation():\n"
	     << "\t tickerId: " << tickerId << "\n"
	     << "\t tickType: " << tickType << "\n"
	     << "\t impliedVol: " << impliedVol << "\n"
	     << "\t delta: " << delta << "\n"
	     << "\t optPrice: " << optPrice << "\n"
	     << "\t pvDividend: " << pvDividend << "\n"
	     << "\t gamma: " << gamma << "\n"
	     << "\t vega: " << vega << "\n"
	     << "\t theta: " << theta << "\n"
	     << "\t undPrice: " << undPrice << "\n";
    dout << for_the_record.str();


}
void PosixTestClient::tickGeneric(TickerId tickerId, TickType tickType, double value) {

    std::stringstream for_the_record;
    for_the_record << "tickGeneric():\n"
//	     << "\t tickerId: " << tickerId << "\n"
	     << "\t tickerId: " << tickerId << " (symbol: " << stockticks[tickerId].sym << ")\n"
	     << "\t tickType: " << tickType << "\n"
	     << "\t value: " << value << "\n";
    dout << for_the_record.str();

}

void PosixTestClient::tickString(TickerId tickerId, TickType tickType, const IBString& value) {
	
    std::stringstream for_the_record;
    for_the_record << "tickString():\n"
//	     << "\t tickerId: " << tickerId << "\n"
	     << "\t tickerId: " << tickerId << " (symbol: " << stockticks[tickerId].sym << ")\n"
	     << "\t tickType: " << tickType << "\n"
	     << "\t value: " << value << "\n";
    dout << for_the_record.str();

}

void PosixTestClient::tickEFP(TickerId tickerId, TickType tickType, double basisPoints, const IBString& formattedBasisPoints,
			   double totalDividends, int holdDays, const IBString& futureExpiry, double dividendImpact, double dividendsToExpiry) {

    std::stringstream for_the_record;
    for_the_record << "tickEFP():\n"
//	     << "\t tickerId: " << tickerId << "\n"
	     << "\t tickerId: " << tickerId << " (symbol: " << stockticks[tickerId].sym << ")\n"
	     << "\t tickType: " << tickType << "\n"
	     << "\t basisPoints: " << basisPoints << "\n"
	     << "\t formattedBasisPoints: " << formattedBasisPoints << "\n"
	     << "\t totalDividends: " << totalDividends << "\n"
	     << "\t holdDays: " << holdDays << "\n"
	     << "\t futureExpiry: " << futureExpiry << "\n"
	     << "\t dividendImpact: " << dividendImpact << "\n"
	     << "\t dividendsToExpiry: " << dividendsToExpiry << "\n";
    dout << for_the_record.str();

}

void PosixTestClient::openOrder( OrderId orderId, const Contract &contract, const Order &order, const OrderState &ostate) {

    std::stringstream for_the_record;
    for_the_record << "openOrder(): " << "\n"
	     << "\t order id: " << orderId << "\n"
	     << "\t contract details: \n"
	     << "\t " << DM::Inspector(&contract) << "\n"
	     << "\t " << DM::Inspector(&order) << "\n"
	     << "\t " << DM::Inspector(&ostate) << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::openOrderEnd() {
    std::stringstream for_the_record;
    for_the_record << "openOrderEnd(): " << "\n";
    dout << for_the_record.str();

}

void PosixTestClient::winError( const IBString &str, int lastError) {
    std::stringstream for_the_record;
    for_the_record << "winError():\n"
	     << "\t str: " << str << "\n"
	     << "\t lastError: " << lastError << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::connectionClosed() {

    std::stringstream for_the_record;
    for_the_record << "connectionClosed(): " << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::updateAccountValue(const IBString& key, const IBString& val,
					  const IBString& currency, const IBString& accountName) {

    std::stringstream for_the_record;
    for_the_record << "updateAccountValue():\n"
	     << "\t key: " << key << "\n"
	     << "\t val: " << val << "\n"
	     << "\t currency: " << currency << "\n"
	     << "\t accountName: " << accountName << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::updatePortfolio(const Contract& contract, int position,
		double marketPrice, double marketValue, double averageCost,
		double unrealizedPNL, double realizedPNL, const IBString& accountName){

    std::stringstream for_the_record;
    for_the_record << "updatePortfolio():\n"
	     << "\t " << DM::Inspector(&contract) << "\n"
	     << "\t position: " << position << "\n"
	     << "\t marketPrice: " << marketPrice << "\n"
	     << "\t marketValue: " << marketValue << "\n"
	     << "\t averageCost: " << averageCost << "\n"
	     << "\t unrealizedPNL: " << unrealizedPNL << "\n"
	     << "\t realizedPNL: " << realizedPNL << "\n"
	     << "\t accountName: " << accountName << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::updateAccountTime(const IBString& timeStamp) {
    std::stringstream for_the_record;
    for_the_record << "updateAccountTime():\n"
	           << "\t timeStamp: " << timeStamp << "\n";
    dout << for_the_record.str();

}

void PosixTestClient::accountDownloadEnd(const IBString& accountName) {
    std::stringstream for_the_record;
    for_the_record << "accountDownloadEnd():\n"
	     << "\t accountName: " << accountName << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::contractDetails( int reqId, const ContractDetails& contractDetails) {

    std::stringstream for_the_record;
    for_the_record << "contractDetails():\n"
     << "\t reqId: " << reqId << "\n"
     << "\t contractDetails: " << DM::Inspector(&contractDetails) << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::bondContractDetails( int reqId, const ContractDetails& contractDetails) {

    std::stringstream for_the_record;
    for_the_record << "bondContractDetails():\n"
	     << "\t reqId: " << reqId << "\n"
	     << "\t contractDetails: " << DM::Inspector(&contractDetails) << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::contractDetailsEnd( int reqId) {
    std::stringstream for_the_record;
    for_the_record << "contractDetailsEnd():\n"
	     << "\t reqId: " << reqId << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::execDetails( int reqId, const Contract& contract, const Execution& execution) {
    std::stringstream for_the_record;
    for_the_record << "execDetails():\n"
	     << "\t contract: " << DM::Inspector(&contract) << "\n"
	     << "\t execution: " << DM::Inspector(&execution) << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::execDetailsEnd( int reqId) {

    std::stringstream for_the_record;
    for_the_record << "execDetailsEnd(): \n"
	     << "\t reqId: " << reqId << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::updateMktDepth(TickerId tickerId, int position, int operation, int side,
									  double price, int size) {
    std::stringstream for_the_record;
    for_the_record << "updateMktDepth():\n"
//	     << "\t id: " << id << "\n"
	     << "\t tickerId: " << tickerId << " (symbol: " << stockticks[tickerId].sym << ")\n"
	     << "\t position: " << position << "\n"
	     << "\t operation: " << operation << "\n"
	     << "\t side: " << side << "\n"
	     << "\t price: " << price << "\n"
	     << "\t size: " << size << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::updateMktDepthL2(TickerId tickerId, int position, IBString marketMaker, int operation,
										int side, double price, int size) {

    std::stringstream for_the_record;
    for_the_record << "updateMktDepthL2():\n"
//	     << "\t id: " << id << "\n"
	     << "\t tickerId: " << tickerId << " (symbol: " << stockticks[tickerId].sym << ")\n"
	     << "\t position: " << position << "\n"
	     << "\t marketMaker: " << marketMaker << "\n"
	     << "\t operation: " << operation << "\n"
	     << "\t side: " << side << "\n"
	     << "\t price: " << price << "\n"
	     << "\t size: " << size << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::updateNewsBulletin(int msgId, int msgType, const IBString& newsMessage, const IBString& originExch) {

    std::stringstream for_the_record;
    for_the_record << "updateNewsBulletin():\n"
	     << "\t msgId: " << msgId << "\n"
	     << "\t msgType: " << msgType << "\n"
	     << "\t newsMessage: " << newsMessage << "\n"
	     << "\t originExch: " << originExch << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::managedAccounts( const IBString& accountsList) {

    std::stringstream for_the_record;
    for_the_record << "managedAccounts():\n"
	     << "\t accountsList: " << accountsList << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::receiveFA(faDataType pFaDataType, const IBString& cxml) {
    std::stringstream for_the_record;
    for_the_record << "receiveFA():\n"
	     << "\t pFaDataType: " << pFaDataType << "\n"
	     << "\t cxml: " << cxml << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::historicalData(TickerId reqId, const IBString& date, double open, double high,
									  double low, double close, int volume, int barCount, double WAP, int hasGaps) {
    std::stringstream for_the_record;
    for_the_record << "historicalData():\n"
	     << "\t reqId: " << reqId << "\n"
    	     << "\t date: " << date << "\n"
	     << "\t open: " << open << "\n"
	     << "\t high: " << high << "\n"
	     << "\t low: " << low << "\n"
	     << "\t close: " << close << "\n"
	     << "\t volume: " << volume << "\n"
	     << "\t barCount: " << barCount << "\n"
	     << "\t WAP: " << WAP << "\n"
	     << "\t hasGaps: " << hasGaps << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::scannerParameters(const IBString &xml) {

    std::stringstream for_the_record;
    for_the_record << "scannerParameters():\n"
	     << "\t xml: " << xml << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::scannerData(int reqId, int rank, const ContractDetails &contractDetails,
	   const IBString &distance, const IBString &benchmark, const IBString &projection,
	   const IBString &legsStr) {

    std::stringstream for_the_record;
    for_the_record << "scannerData():\n"
	     << "\t reqId: " << reqId << "\n"
	     << "\t rank: " << rank << "\n"
	     << "\t contractDetails: " << DM::Inspector(&(contractDetails) ) << "\n"
	     << "\t distance: " << distance << "\n"
	     << "\t benchmark: " << benchmark << "\n"
	     << "\t projection: " << projection << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::scannerDataEnd(int reqId) {

    std::stringstream for_the_record;
    for_the_record << "scannerDataEnd():\n"
	     << "\t reqId: " << reqId << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::realtimeBar(TickerId reqId, long time, double open, double high, double low, double close,
								   long volume, double wap, int count) {

    std::stringstream for_the_record;
    for_the_record << "realtimeBar():\n"
	     << "\t reqId: " << reqId << "\n"
	     << "\t time: " << time << "\n"
	     << "\t open: " << open << "\n"
	     << "\t high: " << high << "\n"
	     << "\t low: " << low << "\n"
	     << "\t close: " << close << "\n"
	     << "\t volume: " << volume << "\n"
	     << "\t wap: " << wap << "\n"
	     << "\t count: " << count << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::fundamentalData(TickerId reqId, const IBString& data) {

    std::stringstream for_the_record;
    for_the_record << "fundamentalData():\n"
	     << "\t reqId: " << reqId << "\n"
	     << "\t data: " << data << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::deltaNeutralValidation(int reqId, const UnderComp& underComp) {

    std::stringstream for_the_record;
    for_the_record << "deltaNeutralValidation(): \n"
	     << "\t reqId: " << reqId << "\n"
	     << "\t underComp: " << DM::Inspector(&underComp) << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::tickSnapshotEnd(int reqId) {

    std::stringstream for_the_record;
    for_the_record << "tickSnapshotEnd():\n"
	     << "\t reqId: " << reqId << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::marketDataType(TickerId reqId, int marketDataType) {

    std::stringstream for_the_record;
    for_the_record << "marketDataType(): " << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::commissionReport( const CommissionReport& commissionReport) {


    std::stringstream for_the_record;
    for_the_record << "commissionReport():" << "\n"
	     << "\t execId: " << commissionReport.execId << "\n"
	     << "\t commission: " << commissionReport.commission << "\n"
	     << "\t currency: " << commissionReport.currency << "\n"
	     << "\t realizedPNL: " << commissionReport.realizedPNL << "\n"
	     << "\t yield: " << commissionReport.yield << "\n"
	     << "\t yieldRedemptionDate (YYYYMMDD format): " << commissionReport.yieldRedemptionDate << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::position( const IBString& account, const Contract& contract, int position, double avgCost) {

    std::stringstream for_the_record;
    for_the_record << "position():\n"
	     << "\t account: " << account << "\n"
	     << "\t contract: " << DM::Inspector(&contract) << "\n"
	     << "\t position: " << position << "\n"
	     << "\t avgCost: " << avgCost << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::positionEnd() {
    std::stringstream for_the_record;
    for_the_record << "positionEnd():\n";
    dout << for_the_record.str();
}

void PosixTestClient::accountSummary( int reqId, const IBString& account, const IBString& tag, const IBString& value, const IBString& curency) {

    std::stringstream for_the_record;
    for_the_record << "accountSummary():\n"
	     << "\t reqId: " << reqId << "\n"
	     << "\t account: " << account << "\n"
	     << "\t tag: " << tag << "\n"
	     << "\t value: " << value << "\n"
	     << "\t curency: " << curency << "\n";
    dout << for_the_record.str();
}

void PosixTestClient::accountSummaryEnd( int reqId) {

    std::stringstream for_the_record;
    for_the_record << "accountSummaryEnd():\n"
	     << "\t reqId: " << reqId << "\n";
    dout << for_the_record.str();
}




void PosixTestClient::example_reqMktData() {

    std::stringstream for_the_record;
    for_the_record << "reqMktData():\n";
    dout << for_the_record.str();

    // subscribe to few tickers
    getContracts();
    for (iterContracts icontract = contracts.begin(); icontract != contracts.end(); icontract++)
    {
        subscribe_to_realtime_ticks(&(*icontract));
    }
}

void PosixTestClient::subscribe_to_realtime_ticks(Contract *contract){

    // todo: check if we already subscribed to the given contract (can it be done by IB's contract id?)
    

    // get symbol from given contract
    std::string symbol;
    if ( contract->symbol.size() > 0) {
	symbol = contract->symbol;
    }
    else if (contract->localSymbol.size() > 0) {
	symbol = contract->localSymbol;
    }
    else
    {
        dout << "subscribe_to_realtime_ticks(): can't get symbol from contract!\n";
        return;
    }
    
    // we set conId field (our local unique ticker id) as a position of this ticker in our stockticks vector
    TickerId tid = (TickerId) stockticks.size();

    // push new ticker into our ticks-tracking vector
    TLTick k;
    k.sym = symbol;
    stockticks.push_back(k);

    dout << "requesting market data for contract:\n" << DM::Inspector(contract) << "\n";
    m_pClient->reqMktData( tid, *contract, "", false);	// reqMktData( id, contract, genericTicks, snapshot);
}


void PosixTestClient::example_placeOrder()
{
    m_state = ST_IDLE;
        dout << "+++ switching state to ST_IDLE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

return;

    std::stringstream for_the_record;
    for_the_record << "example_placeOrder()\n";
    dout << for_the_record.str();

    // getContracts(); no need to call it 2nd time (1st time we called it during reqmktData phase)

    Order order;
    order.action = "BUY";
    order.totalQuantity = 1;
    order.orderType = "MKT"; // "LMT";
    //order.lmtPrice = 0.01;

/*
    dout << "\t Placing Order with orderId: " << m_orderId << "\n"
	 << " action: " << order.action << "\n"
	 << " qty: " << order.totalQuantity << "\n"
	 << " sym: " << contract.symbol << "\n"
	 << " libPrice: " << order.lmtPrice << "\n";
    
    m_state = ST_PLACEORDER_ACK;

    m_pClient->placeOrder( m_orderId, contract, order);
    */
}

void PosixTestClient::getContracts()
{
    // Stock: Facebook
    {
    Contract contract;
    contract.symbol = "FB";
    contract.secType = "STK";
    contract.exchange = "SMART";
//    contract.primaryExchange = "NASDAQ";
    contract.currency = "USD";			// currency must be USD for "FB"
    contracts.push_back(contract);		// Note that, since contracts is a vector of contracts (not pointers to them), the push_back will create a copy of your contract struct
    }

    // forex: CAD.JPY pair example:
    {
    Contract contract;
    contract.symbol = "";			// "CAD.JPY"<- this is no go, leave symbol blank for forex pair
    contract.localSymbol = "CAD.JPY";
    contract.secType = "CASH";
    contract.exchange = "IDEALPRO";
    contract.currency = "JPY";		//  "CAD"<- cad is no go, set 2nd currency name here
    contracts.push_back(contract);
    }

    // Stock: IBM
    {
    Contract contract;
    contract.symbol = "IBM";
    contract.secType = "STK";
    contract.exchange = "SMART";
    contract.currency = "USD";
    contracts.push_back(contract);
    }
}
