/* Copyright (C) 2013 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 * and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable. */

#ifdef _WIN32
# include <Windows.h>
# define sleep( seconds) Sleep( seconds * 1000);
#else
# include <unistd.h>
#endif

#include "PosixTestClient.h"
#include "dm.h"

const unsigned MAX_ATTEMPTS = 50;
const unsigned SLEEP_TIME = 10;

#include <fstream>
#include <iostream>
std::ofstream anomaly_fstream;

#include <signal.h>
void interuptHandler(int s){
    //http://stackoverflow.com/questions/1641182/how-can-i-catch-a-ctrl-c-event-c
    printf("Inteerrupted by user. Caught signal %d\n", s);
    anomaly_fstream.flush(); // if ofstream was written with string (without std::endl) we might loose this if interrupted, so flusing now just to be safe.
    exit(1); 
}

int main(int argc, char** argv)
{
   // we'll need to catch Ctrl-C to flush some ofstreams
   struct sigaction sigIntHandler;
   sigIntHandler.sa_handler = interuptHandler;
   sigemptyset(&sigIntHandler.sa_mask);
   sigIntHandler.sa_flags = 0;
   sigaction(SIGINT, &sigIntHandler, NULL);

    // prepare anomaly ofstream. Usage example:
    // 		anomaly << "just a test record to see if it works...\n";
    anomaly_fstream.open( ANOMALY_FULL_FILENAME, std::ios_base::app | std::ios_base::out);
    anomaly_fstream.rdbuf()->pubsetbuf(0, 0);

    const char* host = argc > 1 ? argv[1] : "";
    unsigned int port = 7496;
    int clientId = 0;

    unsigned attempt = 0;
    printf( "Start of POSIX Socket Client Test %u\n", attempt);

    for (;;) {
		++attempt;
		printf( "Attempt %u of %u\n", attempt, MAX_ATTEMPTS);

		PosixTestClient client;

		client.connect( host, port, clientId);

		while( client.isConnected()) {
			client.processMessages();
		}

		if( attempt >= MAX_ATTEMPTS) {
			break;
		}

		printf( "Sleeping %u seconds before next attempt\n", SLEEP_TIME);
		sleep( SLEEP_TIME);
    }

    printf ( "End of POSIX Socket Client Test\n");
}

