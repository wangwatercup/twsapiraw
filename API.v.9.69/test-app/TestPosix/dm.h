#ifndef dm_h__INCLUDED
#define dm_h__INCLUDED

#include <sstream>      // std::stringstream

#include "../shared/Contract.h"
#include "../shared/Order.h"
#include "../shared/OrderState.h"
#include "../shared/Execution.h"

// note: if DEBUG is defined, then dout << "some debug info"; will generate output to stderr#ifdef DEBUG
#define DEBUG	

// for measuring high-resolution-time (by calling: clock_gettime())
// http://stackoverflow.com/questions/538609/high-resolution-timer-with-c-and-linux
#include <sys/time.h>

//------------------------------------------------------------- only for osx compatability (begin)
// http://stackoverflow.com/questions/5167269/clock-gettime-alternative-in-mac-os-x
#ifdef __MACH__
#define CLOCK_REALTIME 0
#define CLOCK_MONOTONIC 0
#include <sys/time.h>
//clock_gettime is not implemented on OSX
int clock_gettime(int /*clk_id*/, struct timespec* t);
#endif
//------------------------------------------------------------- only for osx compatability (end)
// following macro is intended for turning debug output on/off (compile-time).
// Usage example in a code:
//	dout << "some debug output var=" << var << "\n";
//
// Example output:
//	21:46:26 PosixTestClient.cpp:208 some debug output var=1234
//
#ifdef DEBUG
#define dout std::cout << DM::HHMMSS() << " " << __FILE__ << ":" << __LINE__ << " "
#else
#define dout 0 && std::cout
#endif

#include <ios>
#include <fstream>
// for now let us collect all unusual in one place
extern std::ofstream anomaly_fstream;
#define ANOMALY_FULL_FILENAME "~/tws_api_anomalies.log"
#define anomaly anomaly_fstream << DM::HHMMSS() << " " << __FILE__ << ":" << __LINE__ << " "




// #include "TLTick.h"
// extern class TLTick;




//
// I've moved all my functionality into separate namespace 
// for easier/cleaner separation of apples from oranges ;)
//
//
namespace DM {

//
// timespec_substract() allows us to measure deltas of high-resolution timer values (to measure how long
// some things took to happen).
//
// Usage example:
/*
    #include "dm.h"
    int main(int argc, char** argv)
    {
        // http://stackoverflow.com/questions/538609/high-resolution-timer-with-c-and-linux
        // clock_gettime(CLOCK_MONOTONIC, &ts); // Works on FreeBSD

        timespec high_resolution_ts1;		// start time
        timespec high_resolution_ts2, delta_ts; // finish time and delta

        clock_gettime(CLOCK_REALTIME, &high_resolution_ts1); // store start time in ts1
        sleep(1);
        clock_gettime(CLOCK_REALTIME, &high_resolution_ts2); // store end time in ts2

        DM::timespec_substract( &delta_ts, &high_resolution_ts2, &high_resolution_ts1); // calcuate delta_ts

        printf( "\tdelta: %lld.%.9ld sec\n", (long long)delta_ts.tv_sec, delta_ts.tv_nsec);	// http://stackoverflow.com/questions/8304259/formatting-struct-timespec
        return 0;
    }
*/
// Output example:
//	delta: 1.000454000 sec
//
//
void timespec_substract(struct timespec *result,
             const struct timespec *lhs,
             const struct timespec *rhs);


// std::string formatting like sprintf
// http://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf
std::string string_format(const std::string fmt_str, ...);


//
// Class HHMMSS is here just to facilitate debug output (by timestamping it!)
// see "#define dout" macro for more details
//
// Usage example:
//
// Usage example in a code:
//	dout << "some debug output var=" << var << "\n";    // NOTE: behind the scene "dout <<" will call HHMMSS(), which will produce: "21:46:26" portion of output
//
// Example output:
//	21:46:26 PosixTestClient.cpp:208 some debug output var=1234
//
class HHMMSS
{
    
public:
    friend std::ostream& operator<<(std::ostream& os, const HHMMSS& item) 
    {
	char hhmmss[9];
	time_t current_time;
        time(&current_time);
	struct tm * timeinfo = localtime ( &current_time);
        sprintf( hhmmss, "%02d:%02d:%02d", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);	
        os << hhmmss;
        return os;
    }
};


//
// class Inspector facilitate the task of debug output for complex objects (ex.: contract, order, etc.)
//
class Inspector {

public:

    // objects passed to class constructor as pointers
    // supported types:
    const Contract *contract;
    const ContractDetails *contractDetails;
    const Order *order;
    const OrderState *orderState;
    const Execution *execution;
    const UnderComp *underComp;
//    const TLTick *tick;

    Inspector(){
	reset_pointers();
    }
    
    Inspector(const Contract *contract_param){
	reset_pointers();
	contract = contract_param;
    }

    Inspector(const ContractDetails *contractDetails_param){
	reset_pointers();
	contractDetails = contractDetails_param;
    }

    Inspector(const Order *order_param){
	reset_pointers();
	order = order_param;
    }

    Inspector(const OrderState *orderState_param){
	reset_pointers();
	orderState = orderState_param;
    }
    
    Inspector(const Execution *execution_param){
	reset_pointers();
	execution = execution_param;
    }
    
    Inspector(const UnderComp *underComp_param){
	underComp = underComp_param;
    }
    
/*
    Inspector(const TLTick *tick_param){
	tick = tick_param;
    }
*/

    void reset_pointers()
    {
	contract = NULL;
	contractDetails = NULL;
	order = NULL;
	orderState = NULL;
	execution = NULL;
	underComp = NULL;
//	tick = NULL;
    }

    friend std::ostream& operator<<(std::ostream& os, const Inspector& i)
    {
	i.spit_result_to(os);
	return os;
    }

    std::string align_right(std::string str, int max_pos=20)
    const {
	std::string result = str;
	int need_spaces = max_pos - str.length();
	return std::string(need_spaces > 0 ? need_spaces : 0, ' ') + result;
    }

    //
    void spit_result_to(std::ostream& os) const {
	if (contract) {
	    spit_contract_to(os);
        }
	else if (contractDetails) {
	    spit_contractDetails_to(os);
        }
        else if (order)
        {
    	    spit_order_to(os);
        }
        else if (orderState)
        {
    	    spit_orderState_to(os);
        }
        else if (execution)
        {
    	    spit_execution_to(os);
    	}
    	else if (underComp)
    	{
    	    spit_underComp_to(os);
    	}
    	/*
    	else if (tick)
    	{
    	    spit_tick_to(os);
    	}
    	*/
        else
        {
       	    os << "inspector cant inspect given object type.. please fix!\n";
        }
    }

    void spit_contract_to(std::ostream& os) const {

	os  << align_right("conId: ") << contract->conId << "\n"
	    << align_right("symbol: ") << contract->symbol << "\n"
	    << align_right("secType: ") << contract->secType << "\n"
	    << align_right("expiry: ") << contract->expiry << "\n"
	    << align_right("strike: ") << contract->strike << "\n"
	    << align_right("right: ") << contract->right << "\n"
	    << align_right("multiplier: ") << contract->multiplier << "\n"
	    << align_right("exchange: ") << contract->exchange << "\n"
	    << align_right("primaryExchange: ") << contract->primaryExchange << "\n" // pick an actual (ie non-aggregate) exchange that the contract trades on.  DO NOT SET TO SMART.
	    << align_right("currency: ") << contract->currency << "\n"
	    << align_right("localSymbol: ") << contract->localSymbol << "\n"
	    << align_right("tradingClass: ") << contract->tradingClass << "\n"
	    << align_right("includeExpired: ") << (contract->includeExpired ? "yes" : "no" ) << "\n"
	    << align_right("secIdType: ") << contract->secIdType << "\n"		// CUSIP;SEDOL;ISIN;RIC
	    << align_right("secId: ") << contract->secId << "\n"

	    // COMBOS
	    <<  align_right("comboLegsDescrip: ") << contract->comboLegsDescrip << "\n" // received in open order 14 and up for all combos
	;

	//	// combo legs
	//	typedef std::vector<ComboLegSPtr> ComboLegList;
	//	typedef shared_ptr<ComboLegList> ComboLegListSPtr;
	//	ComboLegListSPtr comboLegs;

	if (contract->comboLegs.get() == NULL)
	{
	    os << align_right("comboLegs: ") << "no legs found\n";
	}
	else
	{
	    Contract::ComboLegList *legs = contract->comboLegs.get();
	    os << align_right("comboLegs: ") << legs->size() << " leg" << (legs->size() > 1 ? "s" : "") <<  " found. Details:\n";
	    for( std::vector<ComboLegSPtr>::iterator ileg = legs->begin(); ileg != legs->end(); ileg++)
	    {
		std::string prefix = std::string(8, ' ');
		os << prefix << align_right("leg number: ") << ileg - legs->begin() << "\n"	// http://stackoverflow.com/questions/2152986/best-way-to-get-the-index-of-an-iterator
		   << prefix << align_right("conId: ") << (*ileg)->conId << "\n"
		   << prefix << align_right("ratio: ") << (*ileg)->ratio << "\n"
		   << prefix << align_right("action: ") << (*ileg)->action << " (note: possible values are: BUY/SELL/SSHORT)\n"    //BUY/SELL/SSHORT   
		   << prefix << align_right("exchange: ") << (*ileg)->exchange << "\n"
		   << prefix << align_right("openClose: ") << (*ileg)->openClose << "\n" // LegOpenClose enum values

		    // for stock legs when doing short sale
		   << prefix << align_right("shortSaleSlot: ") << (*ileg)->shortSaleSlot << " (note: 1 = clearing broker, 2 = third party)\n" // 1 = clearing broker, 2 = third party
		   << prefix << align_right("designatedLocation: ") << (*ileg)->designatedLocation << "\n"
		   << prefix << align_right("exemptCode: ") << (*ileg)->exemptCode << "\n";
		
	    }
	}

/* commented for now since 'underComp' pointer is not set to NULL when we create contract instance
   and so if we'd try to read its field it will core dump on us ;)
   
	if (contract->underComp)
	{
	    os << align_right("underComp: ") << "not set\n";
	}
	else
	{
	    os << align_right("underComp: ") << "is set. Details:\n";
	    std::string prefix = std::string(8, ' ');
	    os << prefix << align_right("conId: ") << contract->underComp->conId << "\n"
	       << prefix << align_right("delta: ") << contract->underComp->delta << "\n"
	       << prefix << align_right("price: ") << contract->underComp->price << "\n";
	}
*/
	
//
//	// delta neutral
//	UnderComp* underComp;
//			long	conId;
//			double	delta;
//			double	price;

        return ;
    }// spit_contract_to()
    
    void spit_order_to(std::ostream& os) const {

	os  << align_right("orderId: ") << order->orderId << "\n"
	    << align_right("clientId: ") << order->clientId << "\n"
	    << align_right("permId: ") << order->permId << "\n"
	    
	    // main order fields
	    << align_right("// main order fields") << "\n"
	    << align_right("totalQuantity: ") << order->totalQuantity << "\n"
	    << align_right("lmtPrice: ") << order->lmtPrice << "\n"
	    << align_right("auxPrice: ") << order->auxPrice << "\n"
	    
	    // extended order fields
	    << align_right("// extended order fields") << "\n"
	    << align_right("ocaType: ") << order->ocaType << "\n"
	    << align_right("transmit: ") << order->transmit << "\n"
	    << align_right("parentId: ") << order->parentId << "\n"
	    << align_right("blockOrder: ") << order->blockOrder << "\n"
	    << align_right("sweepToFill: ") << order->sweepToFill << "\n"
	    << align_right("displaySize: ") << order->displaySize << "\n"
	    << align_right("triggerMethod: ") << order->triggerMethod << "\n"
	    << align_right("outsideRth: ") << order->outsideRth << "\n"
	    << align_right("hidden: ") << order->hidden << "\n"
	    << align_right("allOrNone: ") << order->allOrNone << "\n"
	    << align_right("minQty: ") << order->minQty << "\n"
	    << align_right("percentOffset: ") << order->percentOffset << "\n"
	    << align_right("overridePercentageConstraints: ") << order->overridePercentageConstraints << "\n"
	    << align_right("trailStopPrice: ") << order->trailStopPrice << "\n"
	    << align_right("trailingPercent: ") << order->trailingPercent << "\n"
	    
	    // institutional (ie non-cleared) only
	    << align_right("// institutional (ie non-cleared) only") << "\n"
	    << align_right("openClose: ") << order->openClose << "\n"
	    << align_right("origin: ") << order->origin << "\n"
	    << align_right("shortSaleSlot: ") << order->shortSaleSlot << "\n"
	    << align_right("exemptCode: ") << order->exemptCode << "\n"
	    
	    // SMART routing only
	    << align_right("// SMART routing only") << "\n"
	    << align_right("discretionaryAmt: ") << order->discretionaryAmt << "\n"
	    << align_right("eTradeOnly: ") << order->eTradeOnly << "\n"
	    << align_right("firmQuoteOnly: ") << order->firmQuoteOnly << "\n"
	    << align_right("nbboPriceCap: ") << order->nbboPriceCap << "\n"
	    << align_right("optOutSmartRouting: ") << order->optOutSmartRouting << "\n"
	    
	    // BOX exchange orders only
	    << align_right("// BOX exchange orders only") << "\n"
	    << align_right("auctionStrategy: ") << order->auctionStrategy << "\n"
	    << align_right("startingPrice: ") << order->startingPrice << "\n"
	    << align_right("stockRefPrice: ") << order->stockRefPrice << "\n"
	    << align_right("delta: ") << order->delta << "\n"
	    
	    // pegged to stock and VOL orders only
	    << align_right("// pegged to stock and VOL orders only") << "\n"
	    << align_right("stockRangeLower: ") << order->stockRangeLower << "\n"
	    << align_right("stockRangeUpper: ") << order->stockRangeUpper << "\n"
	    
	    // VOLATILITY ORDERS ONLY
	    << align_right("// VOLATILITY ORDERS ONLY") << "\n"
	    << align_right("volatility: ") << order->volatility << "\n"
	    << align_right("volatilityType: ") << order->volatilityType << " // 1=daily, 2=annual\n"
	    << align_right("deltaNeutralOrderType: ") << order->deltaNeutralOrderType << "\n"
	    << align_right("deltaNeutralAuxPrice: ") << order->deltaNeutralAuxPrice << "\n"
	    << align_right("deltaNeutralConId: ") << order->deltaNeutralConId << "\n"
	    << align_right("deltaNeutralSettlingFirm: ") << order->deltaNeutralSettlingFirm << "\n"
	    << align_right("deltaNeutralClearingAccount: ") << order->deltaNeutralClearingAccount << "\n"
	    << align_right("deltaNeutralClearingIntent: ") << order->deltaNeutralClearingIntent << "\n"
	    << align_right("deltaNeutralOpenClose: ") << order->deltaNeutralOpenClose << "\n"
	    << align_right("deltaNeutralShortSale: ") << order->deltaNeutralShortSale << "\n"
	    << align_right("deltaNeutralShortSaleSlot: ") << order->deltaNeutralShortSaleSlot << "\n"
	    << align_right("deltaNeutralDesignatedLocation: ") << order->deltaNeutralDesignatedLocation << "\n"
	    << align_right("continuousUpdate: ") << order->continuousUpdate << "\n"
	    << align_right("referencePriceType: ") << order->referencePriceType << " // 1=Average, 2 = BidOrAsk\n"

	    // COMBO ORDERS ONLY
	    << align_right("// COMBO ORDERS ONLY") << "\n"
	    << align_right("basisPoints: ") << order->basisPoints << " // EFP orders only\n"
	    ;	    
    }// spit_order_to()


    void spit_orderState_to(std::ostream& os) const {

	os  << align_right("status: ") << orderState->status << "\n"
	    << align_right("initMargin: ") << orderState->initMargin << "\n"
	    << align_right("maintMargin: ") << orderState->maintMargin << "\n"
	    << align_right("equityWithLoan: ") << orderState->equityWithLoan << "\n"
	    << align_right("commission: ") << orderState->commission << "\n"
	    << align_right("minCommission: ") << orderState->minCommission << "\n"
	    << align_right("maxCommission: ") << orderState->maxCommission << "\n"
	    << align_right("commissionCurrency: ") << orderState->commissionCurrency << "\n"
	    << align_right("warningText: ") << orderState->warningText << "\n"
	    ;
    }// spit_orderState_to()

    void spit_contractDetails_to(std::ostream& os) const {

	os  << align_right("summary (contract): ") << DM::Inspector(&(contractDetails->summary)) << "\n"
	    << align_right("marketName: ") << contractDetails->marketName << "\n"
	    << align_right("minTick: ") << contractDetails->minTick << "\n"
	    << align_right("orderTypes: ") << contractDetails->orderTypes << "\n"
	    << align_right("validExchanges: ") << contractDetails->validExchanges << "\n"
	    << align_right("priceMagnifier: ") << contractDetails->priceMagnifier << "\n"
	    << align_right("underConId: ") << contractDetails->underConId << "\n"
	    << align_right("longName: ") << contractDetails->longName << "\n"
	    << align_right("contractMonth: ") << contractDetails->contractMonth << "\n"
	    << align_right("industry: ") << contractDetails->industry << "\n"
	    << align_right("category: ") << contractDetails->category << "\n"
	    << align_right("subcategory: ") << contractDetails->subcategory << "\n"
	    << align_right("timeZoneId: ") << contractDetails->timeZoneId << "\n"
	    << align_right("tradingHours: ") << contractDetails->tradingHours << "\n"
	    << align_right("liquidHours: ") << contractDetails->liquidHours << "\n"
	    << align_right("evRule: ") << contractDetails->evRule << "\n"
	    << align_right("evMultiplier: ") << contractDetails->evMultiplier << "\n"
	    << align_right("secIdList: ");
	    
	    const TagValueListSPtr tv_list = contractDetails->secIdList;
	    if (tv_list.get() == 0)
	    {
		os  << "empty list\n";
	    }
	    else
	    {
		for ( TagValueList::iterator i = tv_list.get()->begin(); i != tv_list.get()->end(); i++)
		{
		    TagValueSPtr tag_val = (*i);
		    os  << "\t key: " << tag_val->tag << "\n"
			<< "\t value: " << tag_val->value << "\n";
		}
	    }
	    
	    // BOND values
	os  << align_right("// BOND values") << "\n"
	    << align_right("cusip: ") << contractDetails->cusip << "\n"
	    << align_right("ratings: ") << contractDetails->ratings << "\n"
	    << align_right("descAppend: ") << contractDetails->descAppend << "\n"
	    << align_right("bondType: ") << contractDetails->bondType << "\n"
	    << align_right("couponType: ") << contractDetails->couponType << "\n"
	    << align_right("callable: ") << contractDetails->callable << "\n"
	    << align_right("putable: ") << contractDetails->putable << "\n"
	    << align_right("coupon: ") << contractDetails->coupon << "\n"
	    << align_right("convertible: ") << contractDetails->convertible << "\n"
	    << align_right("maturity: ") << contractDetails->maturity << "\n"
	    << align_right("issueDate: ") << contractDetails->issueDate << "\n"
	    << align_right("nextOptionDate: ") << contractDetails->nextOptionDate << "\n"
	    << align_right("nextOptionType: ") << contractDetails->nextOptionType << "\n"
	    << align_right("nextOptionPartial: ") << contractDetails->nextOptionPartial << "\n"
	    << align_right("notes: ") << contractDetails->notes << "\n"
	    ;
    }// spit_contractDetails_to()

    void spit_execution_to(std::ostream& os) const {

	os  << align_right("execId: ") << execution->execId << "\n"
	    << align_right("time: ") << execution->time << "\n"
	    << align_right("acctNumber: ") << execution->acctNumber << "\n"
	    << align_right("exchange: ") << execution->exchange << "\n"
	    << align_right("side: ") << execution->side << "\n"
	    << align_right("shares: ") << execution->shares << "\n"
	    << align_right("price: ") << execution->price << "\n"
	    << align_right("permId: ") << execution->permId << "\n"
	    << align_right("clientId: ") << execution->clientId << "\n"
	    << align_right("orderId: ") << execution->orderId << "\n"
	    << align_right("liquidation: ") << execution->liquidation << "\n"
	    << align_right("cumQty: ") << execution->cumQty << "\n"
	    << align_right("avgPrice: ") << execution->avgPrice << "\n"
	    << align_right("orderRef: ") << execution->orderRef << "\n"
	    << align_right("evRule: ") << execution->evRule << "\n"
	    << align_right("evMultiplier: ") << execution->evMultiplier << "\n"
	    ;
    }// spit_execution_to()

    void spit_underComp_to(std::ostream& os) const {	
	os  << align_right("conId: ") << underComp->conId << "\n"
	    << align_right("delta: ") << underComp->delta << "\n"
	    << align_right("price: ") << underComp->price << "\n"
	    ;
    }// spit_underComp_to()

/*
    void spit_tick_to(std::ostream& os) const {	
	os  << align_right("sym: ") << tick->Serialize() << "\n"
	    ;
    }// spit_underComp_to()
*/

};// end of class Inspector

} // end of namespace DM



#endif
