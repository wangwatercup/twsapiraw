This readme file contains only v.9.69-specific notes.
For general info please read general readme :)

[Q] where this stuff came from?
[A] go to: http://interactivebrokers.github.io/#
    accept license agreement
    choose: IB API for Mac/Unix (as of today (Dec-2013) they have Version: API 9.69 Release Date: July 1 2012)
    you'll get file: twsapi_unixmac_969.02.jar
    "unzip" downloaded file: jar xf twsapi_unixmac_969.02.jar
    cd IBJts/samples/TestPosix
    cp Makefile.linux Makefile
    make
    (fix some errors with misplaces header files etc.:)

#################################
#				#
# List of included examples	#
#				#
#################################

directory structure
-------------------
.
└── API.v.9.69
    ├── IBJts                  <--- this whole directory came from IB file: twsapi_unixmac_969.02.jar
    │   ├── samples
    │   │   ├── Java
    │   │   │   ├── apidemo
    │   │   │   │   └── util
    │   │   │   ├── samples
    │   │   │   │   ├── dnhedge
    │   │   │   │   └── rfq
    │   │   │   └── TestJavaClient
    │   │   └── TestPosix
    │   └── source
    │       ├── JavaClient
    │       │   └── com
    │       │       └── ib
    │       │           ├── client
    │       │           ├── contracts
    │       │           └── controller
    │       └── PosixClient
    │           ├── shared
    │           └── src
    └── test-app               <--- we put copy of IB's src + shared + TestPosix here, for
        ├── PosixSocketClient       some experiments and to make it minimal and compilable :)
        │   └── src
        ├── shared
        └── TestPosix           <--- this is our "sandbox" folder, no changes of IB's stuff
                                     outside this folder. Use it to play with some examples.


[Q] Could you describe what is included in examples .cpp files?
[A] Here you go:

----------------------------[ examples file list ]---(begin)--------------------------------------------

    00_original_PosixTestClient.cpp - original IB's client example, no changes whatsoever :)

    01_all_API_funcitons_added_to_PosixTestClient.cpp - added all TWS API functions for now just to generate at least one debug
                                                        message on the screen, so when API calls them it will be visible to you

    02_measure-reqCurrentTime-performance.PosixTestClient.cpp - an attempt to call reqCurrentTime() and
							        measure exact time it takes to get a reply (it happen this f-n 
							        does not send request to remote IB's servers
							        it is just a local call never leaving your host!-)
								see: 02_measure-reqCurrentTime-performance.PosixTestClient.notes.txt

    03_forex_buy_CAD.JPY_PosixTestClient.cpp - this example would subscribe for forex pair CAD.JPY and will start receiving tons of ticks
	(nice thing about this example is that is is woking 24/7, good for "in the middle of the night debugging" sessions ;)

    04_silent_sniffer_PosixTextClient.cpp - run it to listen to all events and place an order using TWS GUI.
					    As a result you'll be able to see all the events details of the order 
					    and all the IB's replies.

----------------------------[ examples file list ]---(end)--------------------------------------------

    All these simplistic examples have similar behaviour "state machine" logic:
	- Main.cpp would create PosixTestClient instance and would connect to TWS

		PosixTestClient client;

		client.connect( host, port, clientId);

		while( client.isConnected()) {		// then here, in the "endless loop" they call processMessage()
			client.processMessages();
		}


	- upon successfull connection API would call function PosixTestClient::nextValidId()
	    which is a bit puzzling since nextValidId() shold be triggered by client request "reqIDs()" (see API refference)

	    IB examples use nextValidId() to switch state:
	    m_state = ST_PING;

	    and then this state change will trigger appropriate case in PosixTestClient::processMessages():
	    (remember - processMessages() is being called from main.cpp endless loop - by far not the best desing, but ok for our needs for now)

	    case ST_PING:
                        reqCurrentTime();
                        break;

	    and inside function PosixTestClient::reqCurrentTime() we finally call IB API:
	            m_pClient->reqCurrentTime();

	    after request is processed by API currentTime() event will be triggered and we'd print out server's time.
	    See void PosixTestClient::currentTime( long time) for details


#################################
#				#
# Commit notes			#
#				#
#################################

rev #1:
-------
- Makefile.mac added along with compile_on_mac script to be able to compile on OSX
- High resolution timer should now show us the time for request-response roundtrip
- Debug output technique now would be to use "dout":
    Example:
        dout << "some debug message with some value=" << value << "\n";
    
    To turn debug on/off (before you compile the code) see PosixTestClient.h
    
	// note: if DEBUG is defined, then dout << "some debug info"; will generate output to stderr#ifdef DEBUG
	#define DEBUG	

- Added all the IB API functions with almost empty bodies to be able to track when API call them for us
  example:

    void PosixTestClient::position( const IBString& account, const Contract& contract, int position, double avgCost) {
	dout << " position(): " << "\n";
    }

    void PosixTestClient::positionEnd() {
	dout << " positionEnd(): " << "\n";
    }

